package br.com.itau.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "CARTAO",configuration = CartaoPagamentoConfiguracao.class)
public interface CartaoPagamento  {

    @GetMapping("/{id}")
    Cartao buscarCartaoPeloId(@PathVariable int id);


}
