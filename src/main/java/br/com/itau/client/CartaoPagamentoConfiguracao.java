package br.com.itau.client;
import feign.Feign;
import feign.RetryableException;
import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;


public class CartaoPagamentoConfiguracao {
    @Bean
    public ErrorDecoder buscarCartaoPeloIdDecoder() {return  new CartaoPagamentoDecoder();}


    @Bean
    public Feign.Builder builder() {
        FeignDecorators decorators = FeignDecorators.builder()
                .withFallback(new CartaoPagamentoFallBack(), RetryableException.class)
                .build();
        return  Resilience4jFeign.builder(decorators);
    }
}
