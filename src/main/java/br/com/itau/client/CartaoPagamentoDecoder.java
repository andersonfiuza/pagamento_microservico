package br.com.itau.client;

import feign.Response;
import feign.codec.ErrorDecoder;

public class CartaoPagamentoDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {

        if(response.status() == 404) {
            return new CartaoNaoEncontrado();
        }
        return errorDecoder.decode(s,response);

    }


}
