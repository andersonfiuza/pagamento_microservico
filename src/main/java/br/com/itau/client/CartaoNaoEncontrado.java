package br.com.itau.client;


import com.netflix.ribbon.proxy.annotation.Http;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND,reason = "Cartão Não Encontrado")
public class CartaoNaoEncontrado extends RuntimeException{
}
