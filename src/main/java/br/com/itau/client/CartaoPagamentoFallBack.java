package br.com.itau.client;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class CartaoPagamentoFallBack implements CartaoPagamento {

    @Override
    public Cartao buscarCartaoPeloId(int id) {
     throw new ResponseStatusException(HttpStatus.SERVICE_UNAVAILABLE, "SERVIÇO INDISPONIVEL");
    }
}
