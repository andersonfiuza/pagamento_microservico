package br.com.itau.DTO;

import br.com.itau.client.Cartao;
import br.com.itau.Model.Pagamento;

import java.util.List;
import java.util.stream.Collectors;

public class ExtratoPagamentoDTO {

    private int id;

    public int getCartaoid() {
        return cartaoid;
    }

    public void setCartaoid(int cartaoid) {
        this.cartaoid = cartaoid;
    }

    private  int cartaoid;
    private  String descricao;
    private double valor;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }


    public ExtratoPagamentoDTO (Pagamento pagamento){

        this.id = pagamento.getId();
        this.descricao = pagamento.getDescricao();
        this.valor = pagamento.getValor();
        this.cartaoid = pagamento.getIdcartao();


    }

    public static  List<ExtratoPagamentoDTO> converter(List<Pagamento> pagamentos){

        return  pagamentos.stream().map(ExtratoPagamentoDTO::new).collect(Collectors.toList());

    }

}
