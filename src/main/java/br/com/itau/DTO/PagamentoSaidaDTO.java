package br.com.itau.DTO;

import br.com.itau.Model.Pagamento;

public class PagamentoSaidaDTO {


    private int id;

    private int cartao_id;
    private double valor;
    private  String descricao;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCartao_id() {
        return cartao_id;
    }

    public void setCartao_id(int cartao_id) {
        this.cartao_id = cartao_id;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }


    public PagamentoSaidaDTO (Pagamento pagamento){

        this.setCartao_id(pagamento.getIdcartao());
        this.setDescricao(pagamento.getDescricao());
        this.setId(pagamento.getId());
        this.setValor(pagamento.getValor());


    }
}
