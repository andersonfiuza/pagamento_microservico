package br.com.itau.DTO;

import br.com.itau.Model.Pagamento;

public class PagamentoEntradaDTO {


    private int cartaoid;
    private double valor;
    private  String descricao;

    private String numero;

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public int getCartaoid() {
        return cartaoid;
    }

    public void setCartaoid(int cartaoid) {
        this.cartaoid = cartaoid;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }


    public Pagamento converterParaPagamento (PagamentoEntradaDTO pagamentoEntradaDTO){
        Pagamento pagamento = new Pagamento();

        pagamento.setDescricao(pagamentoEntradaDTO.getDescricao());
        pagamento.setValor(pagamentoEntradaDTO.getValor());
        return pagamento;

    }

}
