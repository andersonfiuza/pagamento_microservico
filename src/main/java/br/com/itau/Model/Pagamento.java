package br.com.itau.Model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
public class Pagamento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;


    private double valor;

    @NotBlank(message = "Favor informar o nome do produto!")
    private  String descricao;

    private int idcartao;

    public int getIdcartao() {
        return idcartao;
    }

    public void setIdcartao(int idcartao) {
        this.idcartao = idcartao;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }





}

