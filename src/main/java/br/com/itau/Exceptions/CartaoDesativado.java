package br.com.itau.Exceptions;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST,reason = "Cartão Inativo!")
public class CartaoDesativado extends RuntimeException {

}
