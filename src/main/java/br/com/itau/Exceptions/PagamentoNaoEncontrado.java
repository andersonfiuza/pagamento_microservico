package br.com.itau.Exceptions;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND,reason = "Pagamentos para o cartão informado  não Encontrado!")
public class PagamentoNaoEncontrado extends RuntimeException {
}
