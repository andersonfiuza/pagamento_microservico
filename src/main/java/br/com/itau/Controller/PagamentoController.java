package br.com.itau.Controller;

import br.com.itau.DTO.ExtratoPagamentoDTO;
import br.com.itau.DTO.PagamentoEntradaDTO;
import br.com.itau.DTO.PagamentoSaidaDTO;
import br.com.itau.Model.Pagamento;
import br.com.itau.Service.PagamentoService;
import br.com.itau.repositories.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/compra")
public class PagamentoController {

    @Autowired
    PagamentoService pagamentoService;


    //RegistraPagamentoCartao
    @PostMapping("/registrapagamento")
    @ResponseStatus(HttpStatus.CREATED)
    public PagamentoSaidaDTO registraNovoPagamento(@RequestBody @Valid PagamentoEntradaDTO pagamentoEntradaDTO) {

        return pagamentoService.registraNovoPagamento(pagamentoEntradaDTO);

    }

    //Consulta Compras do cartao.

    @GetMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public List<ExtratoPagamentoDTO> listaPagamentos(@PathVariable(name = "id") int id) {
        List<Pagamento> pagamentos = pagamentoService.lertodosospagamentos(id);
        return ExtratoPagamentoDTO.converter(pagamentos);


    }
}
