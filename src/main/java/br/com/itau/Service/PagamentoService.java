package br.com.itau.Service;

import br.com.itau.DTO.PagamentoEntradaDTO;
import br.com.itau.DTO.PagamentoSaidaDTO;
import br.com.itau.Exceptions.CartaoDesativado;
import br.com.itau.Exceptions.PagamentoNaoEncontrado;
import br.com.itau.client.Cartao;
import br.com.itau.Model.Pagamento;
import br.com.itau.client.CartaoPagamento;
import br.com.itau.repositories.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PagamentoService {


    @Autowired
    PagamentoRepository pagamentoRepository;

    @Autowired
    CartaoPagamento cartaoPagamento;

    public PagamentoSaidaDTO registraNovoPagamento(PagamentoEntradaDTO pagamentoEntradaDTO) {

        Cartao cartao = cartaoPagamento.buscarCartaoPeloId(pagamentoEntradaDTO.getCartaoid());


        if (cartao.getAtivo().equals(true)) {
            Pagamento pagamento = new Pagamento();

            pagamento.setIdcartao(cartao.getId());
            pagamento.setDescricao(pagamentoEntradaDTO.getDescricao());
            pagamento.setValor(pagamentoEntradaDTO.getValor());


            pagamento = pagamentoRepository.save(pagamento);

            return new PagamentoSaidaDTO(pagamento);

        } else {
            throw new CartaoDesativado();
        }


    }

    public List<Pagamento> lertodosospagamentos(int id) {

        List<Pagamento> list;

        list = pagamentoRepository.findByidcartao(id);


        if (list.size() == 0) {
            throw new PagamentoNaoEncontrado();

        }
        return list;


    }
}


